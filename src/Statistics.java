import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Statistics {

    static class Sum implements Writable {
        private LongWritable n;
        private DoubleWritable sum;
        private DoubleWritable squaresSum;

        Sum() {
            n = new LongWritable(0);
            sum = new DoubleWritable(0);
            squaresSum = new DoubleWritable(0);
        }

        @Override
        public void write(DataOutput dataOutput) throws IOException {
            n.write(dataOutput);
            sum.write(dataOutput);
            squaresSum.write(dataOutput);
        }

        @Override
        public void readFields(DataInput dataInput) throws IOException {
            n.readFields(dataInput);
            sum.readFields(dataInput);
            squaresSum.readFields(dataInput);
        }

        void add(Double value) {
            long newN = n.get() + 1;
            double newSum = sum.get() + value;
            double newSumSquares = squaresSum.get() + value * value;

            n.set(newN);
            sum.set(newSum);
            squaresSum.set(newSumSquares);
        }

        void add(Sum s) {
            long newN = n.get() + s.n.get();
            double newSum = sum.get() + s.sum.get();
            double newSquaresSum = squaresSum.get() + s.squaresSum.get();

            n.set(newN);
            sum.set(newSum);
            squaresSum.set(newSquaresSum);
        }

        double expectedValue() {
            return expectedValue(false);
        }

        double expectedValue(boolean squared) {
            double sumValue = (squared ? squaresSum : sum).get();
            long nValue = n.get();
            return sumValue / nValue;
        }

        double dispersion() {
            double evSquared = expectedValue(true);
            double ev = expectedValue(false);

            return evSquared - ev * ev;
        }
    }

    private static class MyMapper extends Mapper<Object, Text, Text, Sum> {
        Map<String, Sum> sumBySequenceName = new HashMap<>();

        public void map(Object key, Text value, Context context) {
            StringTokenizer tokenizer = new StringTokenizer(value.toString());

            String sequenceName = tokenizer.nextToken();
            double itemValue = Double.valueOf(tokenizer.nextToken());

            Sum sum;

            if (sumBySequenceName.containsKey(sequenceName)) {
                sum = sumBySequenceName.get(sequenceName);
            } else {
                sum = new Sum();
                sumBySequenceName.put(sequenceName, sum);
            }

            sum.add(itemValue);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            for (Map.Entry<String, Sum> entry : sumBySequenceName.entrySet()) {
                Text key = new Text(entry.getKey());
                Sum value = entry.getValue();
                context.write(key, value);
            }
        }

    }

    private static class MyReducer extends Reducer<Text, Sum, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Sum> values, Context context) throws IOException, InterruptedException {
            Sum sum = new Sum();
            for (Sum s : values)
                sum.add(s);

            String result = sum.expectedValue() + " " + sum.dispersion();
            context.write(key, new Text(result));
        }
    }

    private static class MyCombiner extends Reducer<Text, Sum, Text, Sum> {
        public void reduce(Text key, Iterable<Sum> values, Context context) throws IOException, InterruptedException {
            for (Sum val : values) {
                context.write(key, val);
            }
        }
    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "statistics");
        job.setJarByClass(Statistics.class);
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Sum.class);
        job.setCombinerClass(MyCombiner.class);
        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}