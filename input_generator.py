#!/usr/bin/env python

import random

INPUT_FILE = 'input/input.txt'
SEQUENCE_COUNT = 50
VALUES_PER_SEQUENCE = 10000
TOTAL_VALUES = SEQUENCE_COUNT * VALUES_PER_SEQUENCE
MAX_ABS_VALUE = 10000


def main():
    with open(INPUT_FILE, 'w') as f:
        for _ in xrange(TOTAL_VALUES):
            seq_no = random.randrange(SEQUENCE_COUNT)
            seq_name = 'sequence_{}'.format(seq_no)

            value = random.randint(-MAX_ABS_VALUE, MAX_ABS_VALUE)
            f.write('{seq} {value}\n'.format(seq=seq_name, value=value))


if __name__ == '__main__':
    main()
